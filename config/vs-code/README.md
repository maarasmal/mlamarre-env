Metals requires a JDK, so install that via homebrew:

```
brew install openjdk
```

Next, download and install VS Code.

Launch the app, and then open the command palette (Cmd+Shift+P) and run
the command: `Shell Command: Install 'code' command in PATH`

Quit VS Code and return to the terminal.

Install the extensions via:
```
for pkg in `cat vs-code-extensions.txt`; do code --install-extension $pkg; done
```

The settings.json file should be referenced via a symlink installed to:
```
~/Library/Application Support/Code/User/
```

A number of handy tips and configuration suggestions can be found here:
- https://gist.github.com/olofwalker/9968687e511a83f2b583381fe8226b87
