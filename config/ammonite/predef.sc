// Link this at ~/.ammonite/predef.sc
// Stole this chunk from a gist that Pilquist posted:
// https://gist.github.com/mpilquist/0b1cc1926bddd31c70ad40663acfec8e

// Save as ~/.ammonite/predef.sc
// To use fs2 from ammonite repl, type `load.fs2` from repl prompt.
// You'll get all fs2 & cats imports, ContextShift and Timer instances
// for IO, and a globalBlocker

import $plugin.$ivy.`org.typelevel:::kind-projector:0.13.2`

// Doesn't seem this is needed any more
// if (!repl.compiler.settings.isScala213)
//   repl.load.apply("interp.configureCompiler(_.settings.YpartialUnification.value = true)")

interp.configureCompiler(_.settings.Ydelambdafy.tryToSetColon(List("inline")))

object load {
  def fs2Version(version: String) = {
    repl.load.apply(s"""
      import $$ivy.`co.fs2::fs2-io:$version`, fs2._, fs2.concurrent._, cats._, cats.implicits._, cats.effect._, cats.effect.implicits._, scala.concurrent.duration._

      implicit val ioContextShift: ContextShift[IO] = IO.contextShift(scala.concurrent.ExecutionContext.Implicits.global)
      implicit val ioTimer: Timer[IO] = IO.timer(scala.concurrent.ExecutionContext.Implicits.global)
    """)
    if (!version.startsWith("1")) repl.load.apply("""
      val globalBlocker: Blocker = cats.effect.Blocker.liftExecutionContext(scala.concurrent.ExecutionContext.Implicits.global)
    """)
  }

  def fs2 = fs2Version("2.5.10")
  // def fs2 = fs2Version("3.2.11")
}

// End thievery

//println("Skipping scodec-bits import due to 2.13...")
println("Importing scodec-bits 1.1.34...")
import $ivy.`org.scodec::scodec-bits:1.1.14`

// println("Importing fs2-core 1.1.0-M2...")
// import $ivy.`co.fs2::fs2-core:1.1.0-M2`

// println("Importing cats-effect 2.0.0-RC2...")
// import $ivy.`org.typelevel::cats-effect:2.0.0-RC2`

// println("Importing monocle 1.5.0-cats...")
// import $ivy.`com.github.julien-truffaut::monocle-core:1.5.0-cats`

println("Run 'load.fs2' to load FS2, cats, cats-effect, etc...")

