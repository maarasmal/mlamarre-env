#!/bin/zsh

# Set the MAARASMAL_REPOS env variable to a default if it wasn't already set
if [[ -z "$MAARASMAL_REPOS" ]]; then
    export MAARASMAL_REPOS=$HOME/repos/maarasmal
    echo -e "alias file set MAARASMAL_REPOS variable to $MAARASMAL_REPOS"
fi

# The -G flag colorizes output on Macs, but hides groups from -l style listings on Linux. Ick.
if [[ `uname -s` == "Darwin" ]]; then
    alias ls="ls -G"
    alias ll="ls -alGFh"
    alias df="df -m"
else
    alias ll="ls -alFh"
    alias df="df -BM"
fi

alias la="ls -a"
alias lal="ls -alh"
alias grep="grep --color=auto"
alias egrep="egrep --color=auto"

# Format the contents of the pasteboard as JSON and copy the formatted text back to the pasteboard
alias fmtpbjson="pbpaste | jq . | pbcopy"

if [ -x /usr/local/bin/terraform ]; then
    alias trfm="/usr/local/bin/terraform"
fi

alias dl-youtube-mp3='youtube-dl -x --audio-format mp3 --audio-quality 320K '

alias clear-spotlight-index='sudo mdutil -avE'

# Maven aliases
alias mci="mvn clean install"
alias mdeps="mvn dependency:tree > deps.txt && dos2unix -f deps.txt && vi deps.txt"

#
# Navigation commands only below this point
#

# General navigation
alias godocs='cd $HOME/Documents'
alias godl='cd $HOME/Downloads'
alias godrop='cd $HOME/Dropbox'
alias godevl='cd $HOME/devl'
alias gomylib='cd $HOME/Library'
alias gosyslib='cd /Library'
alias golib='echo "I think you want either gomylib or gosyslib"'

# Non-work repositories
alias gomyrepos='cd $MAARASMAL_REPOS'
alias goenv='cd $MAARASMAL_REPOS/mlamarre-env'
alias gocheat='cd $MAARASMAL_REPOS/cheatsheets'
alias gosandbox='cd $MAARASMAL_REPOS/sbt-sandbox'
alias gofp='cd $MAARASMAL_REPOS/fp-in-scala'
alias goscala3='cd $MAARASMAL_REPOS/learning-scala3'
alias gotoys='cd $MAARASMAL_REPOS/toy-problems'
alias gomtg4s='cd $MAARASMAL_REPOS/mtg4s'

if [[ -d $HOME/repos/github ]]; then
    alias gogithub='cd $HOME/repos/github'
    alias gocats='cd $HOME/repos/github/cats'
    alias gocatseffect='cd $HOME/repos/github/cats-effect'
    alias gofs2='cd $HOME/repos/github/fs2'
    alias goscodecbits='cd $HOME/repos/github/scodec-bits'
fi
