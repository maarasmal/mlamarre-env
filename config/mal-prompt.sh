#!/bin/bash

if [ -r $MLAMARRE_ENV/config/git-prompt.sh ]; then
    . $MLAMARRE_ENV/config/git-prompt.sh
else
    echo "WARN: $0 could not locate the git-prompt.sh script!"
fi

export __NO_COLOR="\[\033[0m\]"
export __RED="\[\033[1;31m\]"
export __GREEN="\[\033[1;32m\]"
export __YELLOW="\[\033[1;33m\]"
export __BLUE="\[\033[1;34m\]"
export __MAGENTA="\[\033[1;35m\]"
export __CYAN="\[\033[1;36m\]"

export __LIGHT_GRAY="\[\033[1;37m\]"
export __DARK_GRAY="\[\033[1;90m\]"
export __LIGHT_RED="\[\033[1;91m\]"
export __LIGHT_GREEN="\[\033[1;92m\]"
export __LIGHT_YELLOW="\[\033[1;93m\]"
export __LIGHT_BLUE="\[\033[1;94m\]"
export __LIGHT_MAGENTA="\[\033[1;95m\]"
export __LIGHT_CYAN="\[\033[1;96m\]"
export __WHITE="\[\033[1;97m\]"

GREEN="\[\e[0;32m\]"
RED="\[\e[0;31m\]"
BLUE="\[\e[1;34m\]"
COLOREND="\[\e[00m\]"


# Responsive Prompt
parse_git_branch() {
	branch=`__git_ps1 "%s"`

	if [[ `tput cols` -lt 110 ]]; then
		branch=`echo $branch | sed s/feature/f/1`
		branch=`echo $branch | sed s/hotfix/h/1`
		branch=`echo $branch | sed s/release/\r/1`

		branch=`echo $branch | sed s/master/mstr/1`
		branch=`echo $branch | sed s/develop/dev/1`
	fi

	if [[ $branch != "" ]]; then
		if [[ $(git status 2> /dev/null | tail -n1) == "nothing to commit, working tree clean" ]]; then
			echo "(${GREEN}$branch${COLOREND}) "
		else
			echo "(${RED}$branch${COLOREND}) "
		fi
	fi
}

working_directory() {
	dir=`pwd`
	in_home=0
	if [[ `pwd` =~ ^"$HOME"(/|$) ]]; then
		dir="~${dir#$HOME}"
		in_home=1
	fi

	workingdir=""
	if [[ `tput cols` -lt 110 ]]; then
		first="/`echo $dir | cut -d / -f 2`"
		letter=${first:0:2}
		if [[ $in_home == 1 ]]; then
			letter="~$letter"
		fi
		proj=`echo $dir | cut -d / -f 3`
		beginning="$letter/$proj"
		end=`echo "$dir" | rev | cut -d / -f1 | rev`

		if [[ $proj == "" ]]; then
			workingdir="$dir"
		elif [[ $proj == "~" ]]; then
			workingdir="$dir"
		elif [[ $dir =~ "$first/$proj"$ ]]; then
			workingdir="$beginning"
		elif [[ $dir =~ "$first/$proj/$end"$ ]]; then
			workingdir="$beginning/$end"
		else
			workingdir="$beginning/…/$end"
		fi
	else
		workingdir="$dir"
	fi

	echo -e "$workingdir "
}

parse_remote_state() {
	remote_state=$(git status -sb 2> /dev/null | grep -oh "\[.*\]")
	if [[ "$remote_state" != "" ]]; then
		out="["

		if [[ "$remote_state" == *ahead* ]] && [[ "$remote_state" == *behind* ]]; then
			behind_num=$(echo "$remote_state" | grep -oh "behind \d*" | grep -oh "\d*$")
			ahead_num=$(echo "$remote_state" | grep -oh "ahead \d*" | grep -oh "\d*$")
			out="$out${RED}$behind_num${COLOREND},${GREEN}$ahead_num${COLOREND}"
		elif [[ "$remote_state" == *ahead* ]]; then
			ahead_num=$(echo "$remote_state" | grep -oh "ahead \d*" | grep -oh "\d*$")
			out="$out${GREEN}$ahead_num${COLOREND}"
		elif [[ "$remote_state" == *behind* ]]; then
			behind_num=$(echo "$remote_state" | grep -oh "behind \d*" | grep -oh "\d*$")
			out="$out${RED}$behind_num${COLOREND}"
		fi

		out="$out]"
		echo "$out "
	fi
}

prompt() {
	if [[ $? -eq 0 ]]; then
		#exit_status="${BLUE}›${COLOREND} "
		exit_status="${BLUE}\$${COLOREND} "
	else
		#exit_status="${RED}›${COLOREND} "
		exit_status="${RED}\$${COLOREND} "
	fi

	# PS1="\h:$(working_directory)$(parse_git_branch)$(parse_remote_state)$exit_status"
	PS1="${__GREEN}\h${__NO_COLOR} [${__CYAN}\t${__NO_COLOR}]:\W $(parse_git_branch)$(parse_remote_state)$exit_status"
}

PROMPT_COMMAND=prompt
