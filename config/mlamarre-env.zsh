#!/bin/zsh

# Check if this shell is interactive
if [[ -o interactive ]]; then
    echo "Running mlamarre-env.zsh..."

    export MLAMARRE_ENV=$HOME/repos/maarasmal/mlamarre-env

    # Add openjdk to the PATH. This assumes that we did `brew install openjdk`
    export PATH="/opt/homebrew/opt/openjdk/bin:$PATH"

    # Add the mlamarre-env bin directory to PATH
    export PATH=$PATH:$MLAMARRE_ENV/bin/:$MLAMARRE_ENV/bin/search

    # Use vim for things like Git and SVN commits.
    export EDITOR=/usr/bin/vim

    # Keep org.codehaus.plexus.classworlds.launcher.Launcher from stealing GUI focus during builds.
    export APPSVR_OPTS="-Djava.awt.headless=true"

    # Add colors to maven.
    # if [ -r $MLAMARRE_ENV/config/mvncolor.sh ]; then
    #     source $MLAMARRE_ENV/config/mvncolor.sh
    #     alias maven="$CCADSD_HOME/maven/bin/mvn"
    # fi

    # Add homebrew version of svn to the path before the standard macOS one.
    # export SVN_HOME=/usr/local//Cellar/subversion/1.10.2/bin
    # export PATH=$SVN_HOME:$PATH

    # 'brew install docbook' told me to add this:
    # export XML_CATALOG_FILES="/usr/local/etc/xml/catalog"

    # Info for using esbt script, see $HOME/bin/esbt for deets
    #export CCAD_USERNAME=mlamarre
    #export ACROPOLIS_ARTIFACTORY_ENC=AP8Z7Mrb4GLxxRi2DSMo8jqh55x
    #export SD_ARTIFACTORY_ENC=APytGBhKVWMJcAp6eYwJjD2y6j

fi

