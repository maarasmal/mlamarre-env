# Basic usage
To use this environment, do the following:

```
mkdir -p ~/repos/maarasmal
cd ~/repos/maarasmal
git clone https://maarasmal@bitbucket.org/maarasmal/mlamarre-env.git
```

If using `zsh` on macOS, you should then be able to just run the following:

```
cd mlamarre-env
./install-for-zsh
```

As of this writing, that script should configure all of the areas covered by this repo, with
the exception of VS Code, which will still need some manual configuration. See the `vs-code`
directory for details.

## Older install instructions for use with Bash

Using this stuff for Bash requires more manual setup, but the "legacy" intructions are below.

On a Mac, add the following to your `~/.bash_profile`:

```
MLAMARRE_ENV=$HOME/repos/maarasmal/mlamarre-env
source $MLAMARRE_ENV/config/bashrc
```

On Linux, you probably just want it in your `~/.bashrc`.

See the following links for details:

- https://apple.stackexchange.com/questions/12993/why-doesnt-bashrc-run-automatically
- http://hayne.net/MacDev/Notes/unixFAQ.html#shellStartup

## one-time-scripts
This directory holds scripts that are more like one-time configuration items. They don't need to be
run every time you start up, and you don't typically want/need them on the `PATH`. To use them,
run whichever one(s) you want individually.

