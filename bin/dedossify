#!/usr/bin/env bash

# Simple script to convert all files under the current directory that have
# DOS style line terminators to have UNIX style line terminators instead.
# Eff you, Windows.

CURRENT_DIR=$(pwd)
DOSFILES=/tmp/dos-files.txt

if ! which dos2unix >> /dev/null
then
    echo "The dos2unix utility was not found on this system. You must install"
    echo "it via homebrew ('brew install dos2unix') to use this program."
    exit 1
fi

echo ""
echo "This script will convert all files found under the current directory"
echo "that have DOS line terminators to have UNIX line terminators instead."
echo "A list of the modified files will be printed when this script"
echo "terminates. No commits/pushes are performed, that's on you."
echo "Current directory: ${CURRENT_DIR}"
echo ""
echo -n "Are you sure you wish to proceed? (y/n) "
read ans
echo ""
ans="`echo $ans | tr '[a-z]' '[A-Z]'`"

if [ "$ans" = "Y" -o "$ans" = "YES" ] ; then

    rm -f $DOSFILES

    # Finds all regular files whose path does not include "/target/" and greps each
    # such non-binary file for a line ending in \r (DOS line endings are CR + LF.
    # '\r' is a CR; the '$' in the grep command matches the end of line on Unix systems,
    # and can be thought of as the LF, or '\n')
    find . -type f -not -path "*/target/*" -print0 | xargs -0 grep -Il '\r$' >> $DOSFILES

    # Call dos2unix for each file in DOSFILES
    for x in `sort ${DOSFILES} | uniq`
    do
        dos2unix "$x"
    done

else
    echo "No files were converted to UNIX line terminators"
fi

