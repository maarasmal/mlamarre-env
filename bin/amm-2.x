#!/bin/bash
#
# Simple helper for launching an Ammonite repl via scala-cli with the Scala version set to 2.x

EXE_NAME=scala-cli

if which $EXE_NAME
then
  $EXE_NAME repl -S 2.13.6 --amm
else
  echo "ERROR: could not locate scala-cli on your \$PATH, exiting..."
  exit 1
fi

