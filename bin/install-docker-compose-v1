#!/bin/bash
#
# The scala-docker-compose plugin relies on docker-compose-v1. Installing that is a challenge these
# days. This script simply wraps the necessary command for you.
#
# Note that docker-compose-v1 was not built for Apple Silicon, so this script will download the
# x86_64 version.
#
# Also note that in testing this out on macOS Monterrey, a simple `docker-compose -v` took 5+
# seconds to run. After researching online, this seems to be due to macOS security scanning the
# binary every time it is run, since it isn't from a "trusted developer". If you use, say, the
# docker-compose that comes with the official DD for Mac (Docker.app/Contents/Resources/bin/...
# or something like that), there is no such speed penalty.
#
# On linux, the file downloaded by this script should work with no performance issues.
#
# Script adapted from advice found here: https://phoenixnap.com/kb/install-docker-compose-on-ubuntu-20-04
#
# Last docker-compose-v1 release: https://github.com/docker/compose/releases/tag/1.29.2

VERSION=1.29.2
INSTALL_LOCATION=$HOME/bin/docker-compose-v1

sudo curl -L "https://github.com/docker/compose/releases/download/$VERSION/docker-compose-$(uname -s)-x86_64" \
  -o $INSTALL_LOCATION

if [ $? -eq 0 ]; then
  sudo chmod 755 $INSTALL_LOCATION
  echo "Downloaded docker-compose $VERSION and installed it as $INSTALL_LOCATION"
  echo "You might want to symlink this as 'docker-compose', like so:"
  echo ""
  echo "  ln -s $INSTALL_LOCATION /usr/local/bin/docker-compose"
else
  echo "ERROR: could not download docker-compose version = $VERSION"
  exit 1
fi

