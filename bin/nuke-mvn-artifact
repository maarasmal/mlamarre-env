#!/usr/bin/env bash
# Usage:
#   nuke-mvn-artifact <groupId> <artifactId> [<version>]
#
# Deletes the specified artifact from the users local Maven repository

print_usage() {
    echo ""
    echo "Usage: "
    echo "  nuke-mvn-artifact <groupId> <artifactId> [<version>]"
    echo ""
}

if [ $# -lt 2 ]; then
    echo "ERROR: insufficient number of arguments"
    print_usage
    exit 1
fi

GROUP_ID=$1
ARTIFACT_ID=$2

if [ $# -eq 3 ]; then
    VERSION=$3
fi

M2_REPO=$HOME/.m2/repository

# Convert the dots in the groupId into slashes
GROUP_ID_DIR=$(echo $GROUP_ID | tr . /)

if [ $# -eq 2 ]; then
    TARGET_DIR=$M2_REPO/$GROUP_ID_DIR/$ARTIFACT_ID
else
    TARGET_DIR=$M2_REPO/$GROUP_ID_DIR/$ARTIFACT_ID/$VERSION
fi

if [ -d $TARGET_DIR ]; then
    echo "Deleting directory: $TARGET_DIR"
    rm -rf $TARGET_DIR
else
    echo "WARNING: could not locate the specified artifact at:"
    echo "  $TARGET_DIR"
    echo ""
    echo "No changes were made"
fi
